package ;

import com.willna.haxeui.bug10.TooMuch;
import haxe.ui.HaxeUIApp;
import com.willna.haxeui.MainView;

class Main {
    private static var app:HaxeUIApp;

    private static var mainV:MainView;
    public static function main() {
        var app = new HaxeUIApp();
        app.ready(function() {
            app.addComponent(new MainView());
            app.start();
        });
    }    
}
