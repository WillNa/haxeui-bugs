package com.willna.haxeui;

import haxe.ui.containers.dialogs.MessageBox.MessageBoxType;
import haxe.ui.core.Screen;
import haxe.ui.events.ItemEvent;
import haxe.ui.containers.Box;
import haxe.ui.core.Component;

import com.willna.haxeui.SubView.SubViewEvent;
import com.willna.haxeui.bug1.*;


@:build(haxe.ui.macros.ComponentMacros.build("assets/main.xml"))
class BugSelectionView extends Box {
    public var bugID:Int = -1;

    @:bind(lv1, ItemEvent.COMPONENT_EVENT)
    public function onSelectBug(e:ItemEvent):Void
    {
        bugID = lv1.selectedIndex;
    }

    public function new() {
        super();
        //100% of ...what ?
        this.percentHeight=100;
        this.percentWidth=100;
    }
}