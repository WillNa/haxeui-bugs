package com.willna.haxeui.bug2;

import haxe.ui.core.Screen;
import haxe.ui.containers.dialogs.MessageBox.MessageBoxType;
import haxe.ui.events.ItemEvent;
import com.willna.haxeui.SubView.SubViewEvent;
import haxe.ui.events.MouseEvent;

@:build(haxe.ui.macros.ComponentMacros.build("assets/wrongselectedindex.xml"))
class WrongSelectedIndexView extends SubView{

    @:bind(lv1, ItemEvent.COMPONENT_EVENT)
    function onItemChange(e) {
        lb.text = "SelectedIndex = "+lv1.selectedIndex;
        Screen.instance.messageBox("Selected :"+lv1.selectedIndex+"\nNotice how the listView isn't refreshed/selected yet\nClose this dialog to update it", "Information", MessageBoxType.TYPE_INFO);               
    }

    @:bind(button1, MouseEvent.CLICK)
    function onButton1(e) {
        dispatch(new SubViewEvent(SubViewEvent.CLOSE));
    }

    public function new() {
        super();
    }
}