package com.willna.haxeui.bug3;

import com.willna.haxeui.SubView.SubViewEvent;
import haxe.ui.events.MouseEvent;

@:build(haxe.ui.macros.ComponentMacros.build("assets/nosize.xml"))
class NoSizeView extends SubView{
    @:bind(button1, MouseEvent.CLICK)
    function onButton1(e) {
        dispatch(new SubViewEvent(SubViewEvent.CLOSE));
    }

    public function new() {
        super();
    }
}