package com.willna.haxeui;

import haxe.ui.containers.dialogs.Dialog;
import haxe.ui.containers.dialogs.MessageBox.MessageBoxType;
import haxe.ui.core.Screen;
import haxe.ui.events.ItemEvent;
import haxe.ui.containers.Box;
import haxe.ui.core.Component;

import com.willna.haxeui.SubView.SubViewEvent;
import com.willna.haxeui.bug1.*;
import com.willna.haxeui.bug2.*;
import com.willna.haxeui.bug3.*;
import com.willna.haxeui.bug7.*;
import com.willna.haxeui.bug8.*;
import com.willna.haxeui.bug9.*;
import com.willna.haxeui.bug10.*;
import com.willna.haxeui.bug11.*;
import com.willna.haxeui.bug12.*;

class MainView extends Box {
    private var selector:BugSelectionView;

    private var currentView:SubView;
    private var bugID:Int = -1;

      public function onSelectBug(e:ItemEvent):Void
    {
        bugID = selector.bugID;
        //trace(bugID);
        switch (bugID)
        {
            case -1:
                Screen.instance.messageBox("Due to a bug in ListView, selected item isn't the current one but the previous.\n See bug 2 for more information and testing", "Error", MessageBoxType.TYPE_ERROR);
                return;
            case 0:
                currentView = new Screen1();
                currentView.registerEvent(SubViewEvent.CLOSE, onCloseView);
                addComponent(currentView);
                //break;
            case 1:
                currentView = new WrongSelectedIndexView();
                currentView.registerEvent(SubViewEvent.CLOSE, onCloseView);
                addComponent(currentView);
                //break;
            case 2:
                currentView = new NoSizeView();
                currentView.registerEvent(SubViewEvent.CLOSE, onCloseView);
                addComponent(currentView);
                //break;
            case 3:
                var dialog = new LayoutBugDialog();
                dialog.showDialog(false);
                return;
            case 4:
                var dialog = new CustomButtonBugDialog();
                dialog.showDialog(false);
                return; 
            case 5:
                var dialog = new BuildBugDialog();
                dialog.showDialog(false);
                return;
            case 6:
                currentView = new MenuTest();
                currentView.registerEvent(SubViewEvent.CLOSE, onCloseView);
                addComponent(currentView);
                //break;
            case 7:
                currentView = new ListViewImage();
                currentView.registerEvent(SubViewEvent.CLOSE, onCloseView);
                addComponent(currentView);
                //break;
            case 8:
                currentView = new RestrictedChar();
                currentView.registerEvent(SubViewEvent.CLOSE, onCloseView);
                addComponent(currentView);
                //break;
            case 9:
                currentView = new TooMuch();
                currentView.registerEvent(SubViewEvent.CLOSE, onCloseView);
                addComponent(currentView);
                //break;
            case 10:
                currentView = new TooSmall();
                currentView.registerEvent(SubViewEvent.CLOSE, onCloseView);
                addComponent(currentView);
                //break;
            case 11:
                currentView = new NoTabBar();
                currentView.registerEvent(SubViewEvent.CLOSE, onCloseView);
                addComponent(currentView);
                //break;
            default:
                //nothing
        }

        selector.hide();
    }

    public function onCloseView(e:SubViewEvent)
    {
        if (currentView != null)
        {
            currentView.unregisterEvents();
            removeComponent(currentView);
            currentView =null;

            //do nothing
            //currentView.destroyComponent();
            //this.screen.removeComponent(screen1);

            //trick to avoid crash but not perfect
            //screen1.hide();
        }
       
        //sorry,horrible hack but fast code ;)
        if (bugID == 0){
            currentView = new Screen2();
            currentView.registerEvent(SubViewEvent.CLOSE, onCloseView);
            addComponent(currentView);
            bugID = -1;
            return;
        }

        selector.show();
    }


    public function new() {
        super();
        
        //100% of ...what ?
        this.percentHeight=100;
        this.percentWidth=100;

        selector = new BugSelectionView();
        selector.registerEvent(ItemEvent.COMPONENT_EVENT, onSelectBug);

        addComponent(selector);
    }
}

class LayoutBugDialog extends Dialog {
    public function new() {
        super();
        title = "Custom button at wrong position";
        buttons = DialogButton.CANCEL | "Custom Button" | DialogButton.APPLY;
    }
}

class CustomButtonBugDialog extends Dialog {
    public function new() {
        super();
        title = "Translation button doesn't work";
        //this doesn't works whil DialogButton is String, Haxe compiler hangs because it detects Int ?!?
        //buttons = "OK" | "KO"; 
    }
}


@:build(haxe.ui.macros.ComponentMacros.build("assets/builddialog.xml"))
class BuildBugDialog extends Dialog {
    public function new() {
        super();
        title = "Build macro not working at first";

        //uncomment this to make it works
        //var l:Label = new Label();
        //l.text =“it only works this way”; //unfortunately, it won’t work with a empty label :(
        //this.addComponent(l);
        buttons = DialogButton.CANCEL | DialogButton.OK;
    }
}
