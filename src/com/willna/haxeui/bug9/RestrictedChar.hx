package com.willna.haxeui.bug9;

import com.willna.haxeui.SubView.SubViewEvent;
import haxe.ui.core.Component;
import haxe.ui.events.MouseEvent;
import haxe.ui.components.Button;


@:build(haxe.ui.macros.ComponentMacros.build("assets/restrictedchar.xml"))
class RestrictedChar extends SubView {
    @:bind(but1, MouseEvent.CLICK)
    function onButton(e) {
        dispatch(new SubViewEvent(SubViewEvent.CLOSE));
    }

    public function new() {
        super();
    }
}