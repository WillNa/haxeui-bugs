package com.willna.haxeui.bug10;

import haxe.ui.events.UIEvent;
import haxe.ui.containers.Box;
import com.willna.haxeui.SubView.SubViewEvent;
import haxe.ui.core.Component;
import haxe.ui.events.MouseEvent;
import haxe.ui.components.Button;


@:build(haxe.ui.macros.ComponentMacros.build("assets/crashcreate.xml"))
class CrashView extends Box {

    /*
    function onValidRestrictedTF(e) {
        //'try' to handle cast exception
        try
        {
            var tf:TextField = cast( e.target, TextField);
            if (tf.restrictChars == "") return;

            var formated:String ="";

           
            //var regexp:EReg = cast(tf.behaviours.find("restrictChars"), RestrictCharsBehaviour).regexp;  // TODO: seems like a crappy way to handle restrict chars
            //if (regexp != null) {
            //    formated = regexp.replace(tf.text, "");
            //}
            
            var ff:Array<String> = tf.text.split("");
           
            for(c in ff)
            {
                if ( tf.restrictChars.indexOf(c) >= 0  )
                    formated += c;
            }
            
            trace("should be "+formated);

            //tf.value = formated;

            //borrowed from TextFieldHelper but it doesn't work
            //tf.getTextInput().text = '${formated}';
            //tf.invalidateComponentLayout();
        }
        catch(e:Dynamic)
        {

        }
    }

    */

    public function new(){
        super();

        this.percentHeight = 100;
        this.percentWidth = 100;
//#if cpp
        //tf.registerEvent(UIEvent.CHANGE, onValidRestrictedTF);
//#end
    }
}

@:build(haxe.ui.macros.ComponentMacros.build("assets/crashintro.xml"))
class CrashIntroduction extends Component {

    //@:bind(project_new, MouseEvent.CLICK)
    function onNew(e) {
       dispatch(new UIEvent("projectNew"));
    }

    public function new() {
        super();

        this.percentHeight = 100;
        this.percentWidth = 100;

        project_new.registerEvent(MouseEvent.CLICK, onNew);
    }
}



@:build(haxe.ui.macros.ComponentMacros.build("assets/toomuch.xml"))
class TooMuch extends SubView {

    private var currentView:Component;

    function onNewProject(e) {
        if (currentView != null) 
        {
            currentView.unregisterEvents();
            this.removeComponent(currentView);
            currentView = null;
        }
        
        //currentView = new ProjectCreate( );
        currentView = new CrashView();
        addComponent( currentView );
    }
   

    private function addIntro(){
        currentView = new CrashIntroduction();
        currentView.registerEvent("projectNew", onNewProject);
        addComponent(currentView);
    }



    public function new() {
        super();
        this.percentHeight = 100;
        this.percentWidth = 100;

        addIntro();
    }
    /*
    @:bind(but1, MouseEvent.CLICK)
    function onButton(e) {
        dispatch(new SubViewEvent(SubViewEvent.CLOSE));
    }
    */
}