package com.willna.haxeui.bug7;

import com.willna.haxeui.SubView.SubViewEvent;
import haxe.ui.events.MouseEvent;

@:build(haxe.ui.macros.ComponentMacros.build("assets/menutest.xml"))
class MenuTest extends SubView{
    @:bind(button1, MouseEvent.CLICK)
    function onButton1(e) {
        dispatch(new SubViewEvent(SubViewEvent.CLOSE));
    }

    public function new() {
        super();
    }
}