package com.willna.haxeui.bug11;

import haxe.ui.events.UIEvent;
import haxe.ui.containers.Box;
import com.willna.haxeui.SubView.SubViewEvent;
import haxe.ui.core.Component;
import haxe.ui.events.MouseEvent;
import haxe.ui.components.Button;


@:build(haxe.ui.macros.ComponentMacros.build("assets/toosmall.xml"))
class TooSmall extends SubView {

    @:bind(button1, MouseEvent.CLICK)
    function onButton(e) {
        dispatch(new SubViewEvent(SubViewEvent.CLOSE));
    }
    
    public function new() {
        super();
    }
    
}