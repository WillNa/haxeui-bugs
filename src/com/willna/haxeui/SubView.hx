package com.willna.haxeui;

import haxe.ui.events.UIEvent;
import haxe.ui.core.Component;


class SubViewEvent extends UIEvent {
    public static inline var CLOSE:String = "closeView";

    public override function clone():SubViewEvent {
        var c:SubViewEvent = new SubViewEvent(this.type);
        c.type = this.type;
        c.bubble = this.bubble; 
        c.target = this.target;
        c.data = this.data;
        c.canceled = this.canceled;

        postClone(c);
        return c;
    }
}

class SubView extends Component {
    public function new() {
        super();

        //100% of what ?
        this.percentHeight = 100;
        this.percentWidth = 100;

        this.paddingTop = 30;
    }
}