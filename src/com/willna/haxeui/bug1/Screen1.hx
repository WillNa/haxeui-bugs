package com.willna.haxeui.bug1;

import haxe.ui.events.UIEvent;
import haxe.ui.core.Component;
import haxe.ui.events.MouseEvent;
import haxe.ui.components.Button;
import com.willna.haxeui.SubView;


@:build(haxe.ui.macros.ComponentMacros.build("assets/first.xml"))
class Screen1 extends SubView{
    @:bind(button1, MouseEvent.CLICK)
    function onButton1(e) {
        dispatch(new SubViewEvent(SubViewEvent.CLOSE));
    }

    public function new() {
        super();
    }
}