package com.willna.haxeui.bug8;

import haxe.ui.components.Image;
import com.willna.haxeui.SubView.SubViewEvent;
import haxe.ui.events.MouseEvent;

@:build(haxe.ui.macros.ComponentMacros.build("assets/listviewimage.xml"))
class ListViewImage extends SubView{
    private var img:Image;

    @:bind(add, MouseEvent.CLICK)
    function onAdd(e) {
        lv1.dataSource.allowCallbacks = false;
        //this work
        lv1.dataSource.add({ 
            value: "Bug" + (lv1.dataSource.size + 1),
            image: "assets/icons/bug.png"
        });
        //this doesn't
        lv1.dataSource.add({ 
            value: "Bug" + (lv1.dataSource.size + 1),
            image: img
        });
        //of cours, img.resource will work, but it's not what come to mind first

        lv1.dataSource.allowCallbacks = true;
    }


    @:bind(button1, MouseEvent.CLICK)
    function onButton1(e) {
        dispatch(new SubViewEvent(SubViewEvent.CLOSE));
    }

    public function new() {
        super();

        img = new Image();
        img.resource = "assets/icons/bug.png";
    }
}